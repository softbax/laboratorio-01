package ar.utn.dds.labo01;

public class RepetidoException extends Exception {
	
	private String repetido; 
	
	public RepetidoException(String nombre) {
		this.repetido = nombre;
	}

	@Override
	public String toString() {
		return "RepetidoException [repetido=" + repetido + "]";
	}
	
	

}
